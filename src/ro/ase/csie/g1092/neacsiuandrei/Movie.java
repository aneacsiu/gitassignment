package ro.ase.csie.g1092.neacsiuandrei;

public class Movie implements Streamable {
	
	String movieTittle;
	float movieLengthInMinutes;
	
	public Movie(String movieTittle, float movieLength) {
		super();
		this.movieTittle = movieTittle;
		this.movieLengthInMinutes = movieLength;
	}

	@Override
	public void showStreamable() {
		System.out.println(movieTittle + " can be streamed on Netflix.");
	};
	
	

}
